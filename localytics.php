<?php
/**
 * Localytics - Statistics Module
 *
 * @author    Cedric VANGOUT <vangout.cedric@gmail.com>
 * @copyright 2013-2014 cVNG IE
 * @link      https://bitbucket.org/cvng/localytics
 */

if (!defined('_PS_VERSION_'))
  exit;

require_once(_PS_MODULE_DIR_.'/localytics/core/LocalyticsApi.php');
require_once(_PS_MODULE_DIR_.'/localytics/models/LocalStats.php');

class Localytics extends Module
{
  const PROFILE_ID = '55669202'; // Current Analytics (view) profile ID

  public function __construct()
  {
    $this->name = 'localytics';
    $this->tab = 'analytics_stats';
    $this->version = '1.5';
    $this->author = 'Cedric Vangout';

    $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
    $this->dependencies = array('marketplace');
    $this->need_instance = 0;

    parent::__construct();

    $this->displayName = $this->l('Localytics');
    $this->description = $this->l('Audience locale');

    $this->confirmUninstall = $this->l('Etes-vous sûr que vous voulez désinstaller?');

    if ($this->active && !Configuration::get('GAPI_EMAIL') && !Configuration::get('GAPI_PASSWD'))
      $this->warning = $this->l('Vous devez configurer votre module');
  }

  public function install()
  {
    if (Shop::isFeatureActive())
      Shop::setContext(Shop::CONTEXT_ALL);

    return parent::install() &&
      $this->installTab() &&
      $this->installCharts() &&
      $this->registerHook('displayBackOfficeHeader');
  }

  private function installTab()
  {
    $tab = new Tab();
    $tab->name[$this->context->language->id] = $this->l('Localytics');
    $tab->class_name = 'AdminLocalytics';
    $tab->module = $this->name;
    $tab->id_parent = Tab::getIdFromClassName('AdminParentStats');

    return $tab->add();
  }

  private function installCharts()
  {
    return @copy(_PS_MODULE_DIR_ . '/localytics/charts/charts.swf', _PS_ADMIN_DIR_.'/charts.swf');
  }

  public function uninstall()
  {
    return parent::uninstall() &&
      $this->uninstallTab() &&
      $this->uninstallCharts() &&
      Configuration::deleteByName('GAPI_EMAIL') &&
      Configuration::deleteByName('GAPI_PASSWD') &&
      Configuration::deleteByName('GAPI_PROFILE_ID');
  }

  private function uninstallTab()
  {
    $id_tab = Tab::getIdFromClassName('AdminLocalytics');
    if ($id_tab != 0)
    {
      $tab = new Tab($id_tab);
      $tab->delete();
      return true;
    }
    return false;
  }

  private function uninstallCharts()
  {
    return @unlink(_PS_ADMIN_DIR_ . '/charts.swf');
  }

  public function getContent()
  {
    $output = '<h2>'.$this->displayName.'</h2>';
    if (Tools::isSubmit('submit'.ucfirst($this->name)))
    {
      Configuration::updateValue('GAPI_EMAIL', Tools::getValue('GAPI_EMAIL'));
      Configuration::updateValue('GAPI_PASSWD', Tools::getValue('GAPI_PASSWD'));
      Configuration::updateValue('GAPI_PROFILE_ID', Localytics::PROFILE_ID);

      if (isset($errors) && count($errors))
        $output .= $this->displayError(implode('<br />', $errors));
      else
        $output .= $this->displayConfirmation($this->l('Paramètres mis à jour'));
    }
    return $output.$this->displayForm();
  }

  public function displayForm()
  {
    if(isset($errors))
      $this->context->smarty->assign('errors', $errors);
    else
      $errors = array();
    $this->context->smarty->assign('request_uri', Tools::safeOutput($_SERVER['REQUEST_URI']));
    $this->context->smarty->assign('path', $this->_path);
    $this->context->smarty->assign('GAPI_EMAIL', pSQL(Tools::getValue('GAPI_EMAIL', Configuration::get('GAPI_EMAIL'))));
    $this->context->smarty->assign('GAPI_PASSWD', pSQL(Tools::getValue('GAPI_PASSWD', Configuration::get('GAPI_PASSWD'))));
    $this->context->smarty->assign('submitName', 'submit'.ucfirst($this->name));
    $this->context->smarty->assign('errors', $errors);

    return $this->display(__FILE__, 'views/templates/admin/config.tpl');
  }

  public function hookDisplayBackOfficeHeader()
  {
    $this->context->controller->addJS($this->_path.'js/AC_RunActiveContent.js');
  }
}
