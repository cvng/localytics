<?php
/**
 * Localytics - Statistics Module
 *
 * @author    Cedric VANGOUT <vangout.cedric@gmail.com>
 * @copyright 2013-2014 cVNG IE
 * @link      https://bitbucket.org/cvng/localytics
 */

class AdminLocalyticsController extends ModuleAdminController
{
    public function __construct()
    {
        $this->context = Context::getContext();
        parent::__construct();
    }

    public function initContent()
    {
        $this->display = 'view';
        parent::initContent();
        $this->setTemplate('localytics.tpl');
    }

    public function initToolbarTitle()
    {
        $this->toolbar_title = array_unique($this->breadcrumbs);
    }

    public function initToolbar()
    {
        parent::initToolbar();
    }

    public function renderView()
    {
        $api_config = Configuration::getMultiple(array('GAPI_EMAIL', 'GAPI_PASSWD', 'GAPI_PROFILE_ID'));
        $localytics_api = new LocalyticsApi($api_config['GAPI_EMAIL'], $api_config['GAPI_PASSWD'], $api_config['GAPI_PROFILE_ID']);

        $id_producer = Tools::getValue('id_producer', null);
        $this->context->smarty->assign('id_producer', $id_producer);

        $reporting_scope = Tools::getValue('reporting_scope', 1);
        $this->context->smarty->assign('reporting_scope', $reporting_scope);
        $localytics_api->setReportingScope($reporting_scope);

        $producers_data = Utils::findAllProducerLinks();
        $this->context->smarty->assign('json_data', $producers_data);

        $reporting_scopes = array(1 => 'Mensuel', 3 => 'Trimestre', 6 => 'Semestre', 12 => 'Annuel');
        $this->context->smarty->assign('reporting_scopes', $reporting_scopes);

        if ($reporting_scope == 1)
            $dimensions = array('year', 'week');
        else
            $dimensions = array('year', 'month');

        if (empty($id_producer)) {
            $metrics = 'visits';

            $total_visits = $localytics_api->requestReportData($dimensions, $metrics, $dimensions, null);
            $this->context->smarty->assign('total_visits', $localytics_api->getVisits());

            $this->context->smarty->assign('progress_rate', sprintf('%+d', $localytics_api->getProgressRate()));

            $localytics_api->requestReportData(null, $metrics, null, LocalyticsApi::ALL_PROFILE_FILTER);
            $this->context->smarty->assign('all_profiles_visits', $localytics_api->getVisits());

            $localytics_api->requestReportData(null, $metrics, null, LocalyticsApi::ALL_SHOP_FILTER);
            $this->context->smarty->assign('all_shops_visits', $localytics_api->getVisits());

            $localytics_api->requestReportData(null, $metrics, null, LocalyticsApi::ALL_PRODUCT_FILTER);
            $this->context->smarty->assign('all_products_visits', $localytics_api->getVisits());

            $ranked_products = array();
            $localytics_api->requestReportData('pagePath', $metrics, '-visits', LocalyticsApi::ALL_PRODUCT_FILTER);

            foreach ($localytics_api->getResults() as $result) {
                $product_link = Utils::extractLink($result->getPagePath(), 'PRODUCT');

                if ($id_product = Utils::findProductIdByLink($product_link)) {
                    $tmp = array();

                    $product = new Product($id_product);
                    $tmp['id'] = $id_product;
                    $tmp['name'] = $product->name[$this->context->language->id];
                    $tmp['link'] = $this->context->link->getProductLink($product);
                    $tmp['visits'] = $result->getVisits();
                    $tmp['producer_name'] = $product->getProducteur()->name;

                    $categories = $product->getCategories();
                    $category = Utils::getParentCategory($categories[0], 2);
                    $tmp['category'] = $category->name[1];

                    $ranked_products[] = $tmp;
                }
            }
            $this->context->smarty->assign('best_products', $ranked_products);

            $best_categories = array();
            foreach (Utils::findMostAttractiveCategories($ranked_products) as $id => $occurrence) {
                $tmp = array();

                $category = new Category($id);
                $tmp['name'] = $category->name;
                $tmp['occurrence'] = $occurrence;

                $best_categories[] = $tmp;
            }
            $this->context->smarty->assign('best_categories', $best_categories);

            $ranked_profiles = array();
            $localytics_api->requestReportData('pagePath', $metrics, '-visits', LocalyticsApi::ALL_PROFILE_FILTER);

            foreach ($localytics_api->getResults() as $result) {
                $profile_link = Utils::extractLink($result->getPagePath(), 'PROFILE');

                if ($id_producer = Utils::findProducerIdByLink($profile_link)) {
                    $tmp = array();

                    $producer = new Producteur($id_producer);
                    $tmp['id'] = $id_producer;
                    $tmp['name'] = $producer->name;
                    $tmp['visits'] = $result->getVisits();
                    $tmp['has_phone'] = $producer->telach ? true : false;
                    $tmp['has_shop'] = ($producer->hasProducts() ? (!$producer->shopoff ? true : false) : false);

                    $ranked_profiles[] = $tmp;
                }
            }
            $this->context->smarty->assign('best_profiles', $ranked_profiles);

            $ranked_shops = array();
            $localytics_api->requestReportData('pagePath', $metrics, '-visits', LocalyticsApi::ALL_SHOP_FILTER);

            foreach ($localytics_api->getResults() as $result) {
                $shop_link = Utils::extractLink($result->getPagePath(), 'SHOP');

                if ($id_producer = Utils::findProducerIdByLink($shop_link)) {
                    $tmp = array();

                    $producer = new Producteur($id_producer);
                    $tmp['id'] = $id_producer;
                    $tmp['name'] = $producer->name;
                    $tmp['visits'] = $result->getVisits();
                    $tmp['nb_products'] = count($producer->getProducts());

                    $ranked_shops[] = $tmp;
                }
            }
            $this->context->smarty->assign('best_shops', $ranked_shops);

            $local_stats = new LocalStats();

            $this->context->smarty->assign('nb_tel', (string)($local_stats->ntel()));
            $this->context->smarty->assign('nb_products', (string)($local_stats->nproducts()));
            $this->context->smarty->assign('nb_liens', (string)($local_stats->nliens()));
            $this->context->smarty->assign('nb_producteurs', (string)($local_stats->nproducteurs()));
            $this->context->smarty->assign('nb_boutique', (string)($local_stats->nboutique()));
            $this->context->smarty->assign('nb_mini', (string)($local_stats->nmini()));
            // $this->context->smarty->assign('nb_mailprod', (string)($local_stats->nmailprod()));
            // $this->context->smarty->assign('nb_visitslien', (string)($local_stats->nvisitslien()));
            // $this->context->smarty->assign('nb_avisprod', (string)($local_stats->navisprod()));
            // $this->context->smarty->assign('nb_avisproducteurs', (string)($local_stats->navisproducteurs()));
        }
        elseif (!empty($id_producer)) {
            $producer = new Producteur($id_producer);
            $page_path = $producer->link_url;

            $this->context->smarty->assign('logo_src', Utils::getProducerLogo($id_producer));

            $metrics = 'pageviews';

            $filter = "ga:pagePath=~^/$page_path";
            $total_visits = $localytics_api->requestReportData($dimensions, $metrics, $dimensions, $filter);
            $this->context->smarty->assign('total_visits', $localytics_api->getPageviews());

            $this->context->smarty->assign('progress_rate', sprintf('%+d', $localytics_api->getProgressRate($filter)));

            $filter = "ga:pagePath=~^/$page_path$";
            $localytics_api->requestReportData(null, $metrics, null, $filter);
            $this->context->smarty->assign('profile_visits', $localytics_api->getPageviews());


            $filter = "ga:pagePath==/$page_path/boutique";
            $localytics_api->requestReportData(null, $metrics, null, $filter);
            $this->context->smarty->assign('shop_visits', $localytics_api->getPageviews());

            $filter = 'ga:pagePath=~'.$producer->link_url.'/[0-9]+-[a-zA-Z0-9-]+.html';
            $localytics_api->requestReportData(null, $metrics, null, $filter);
            $this->context->smarty->assign('products_visits', $localytics_api->getPageviews());

            $products = Product::findByProducteur($id_producer);
            $products_data = array();

            foreach ($products as $product) {
                $tmp = array();

                $tmp['id_product'] = $product->id_product;
                $tmp['name'] = $product->name[$this->context->language->id];

                $products_data[] = $tmp;
            }
            $this->context->smarty->assign('products', $products_data);

            $id_product = Tools::getValue('id_product', null);
            $this->context->smarty->assign('id_product', null);

            if (!empty($id_product)) {
                $product = new Product($id_product);

                if ($product->getProducteur()->id == $id_producer) {
                    $this->context->smarty->assign('id_product', $id_product);

                    $product_link = $product->link_rewrite[$this->context->language->id];
                    $filter = 'ga:pagePath=@'.$product_link;
                    $localytics_api->requestReportData(null, $metrics, null, $filter);
                    $this->context->smarty->assign('product_visits', $localytics_api->getPageviews());

                    $this->context->smarty->assign('product_prog_rate', sprintf('%+d', $localytics_api->getProgressRate($filter)));

                    $product_img = Product::getCover($id_product);
                    $product_img_src = $this->context->link->getImageLink($product_link, $product_img['id_image'], 'thickbox_default');
                    $this->context->smarty->assign('product_img_src', $product_img_src);
                }
            }
        }

        LocalyticsApi::outputResultsAsXML($total_visits, _PS_MODULE_DIR_.'localytics/charts/charts_xml/sample.xml', _PS_MODULE_DIR_.'localytics/charts/charts_xml/data.xml');

        parent::renderView();
    }
}
