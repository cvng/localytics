<?php
/**
 * Classe pour les distinctions des producteur
 *
 * @author Mikael SERREAU <mikael@squareeyes.fr>
 */
class LocalStats extends ObjectModel
{
    public $id_data;
    public $ntel; /*Nombre de num�ro de t�l�phone*/
	public $nproducts;/*Nombre de produits en ligne*/
	public $nliens;/*Nombre de liens vers des fiches produits de tiers*/
	public $nproducteurs;/*Nombre de producteurs*/
	public $nboutique;/*Nombre de boutiques*/
	public $nmini;/*Nombre de mini-profil*/
	public $nmailprod;/*Nombre de mail envoy�s au producteurs*/
	public $nvisitslien;/*Nombre de visites des liens tiers*/
	public $navisprod;/*Nombre d'avis produits*/
	public $navisproducteurs;/*Nombre d'avis producteurs*/
	public $date_add;/*Date d'ajout*/


    /**
     * Constructeur
     * @param int $idDistinction
     */
    public function __construct($id_data = null)
    {
        parent::__construct($id_data);

		$this->ntel=$this->ntel();
		$this->nproducts=$this->nproducts();
		$this->nproducteurs=$this->nproducteurs();
		$this->nliens=$this->nliens();
		$this->nboutique=$this->nboutique();
		$this->nmini=$this->nmini();
		if ($id_data) {
            $this->hydrate($this->findOneById($id_data));
       }
    }

    public static $definition = array(
        'table'     => 'localdata',
        'primary'   => 'id_data',
        'multilang' => false,
        'fields'    => array(
            'ntel'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'nproducts'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'nliens'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'nproducteurs'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'nboutique'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'nmini'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'nmailprod'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'nvisitslien'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'navisprod'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'navisproducteurs'  => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => false),
			'date_add' => 					array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat')
        )
    );

    /**
     * Recherche une distinction par ID
     * @param int $idDistinction
     * @return array
     */
    public static function findOneById($id_data)
    {
        $query = new DbQuery();
        $query->select('*');
        $query->from('localdata', 'l');
        $query->where('l.id_data = '.pSQL($id_data));

        return Db::getInstance()->getRow($query);
    }

	public function ntel(){
		$query = new DbQuery();
        $query->select('pr.telach');
        $query->from('producteur', 'pr');
		$query->join('LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = pr.id_customer');
		$query->where('pr.valid = 1');
		$query->where('c.active = 1');
		$query->where('pr.telach IS NOT NULL');

       $results=Db::getInstance()->ExecuteS($query);
		$ntel=0;
		foreach($results as $tel){
			if($tel['telach'] && Validate::isPhoneNumber($tel['telach'])){
				$ntel=$ntel+1;
			}
		}
		return $ntel;

	}

	public function nproducts(){
		$query = new DbQuery();
        $query->select('COUNT(p.id_product)');
        $query->from('product', 'p');
		$query->join('LEFT JOIN '._DB_PREFIX_.'producteur pr ON pr.id_producteur = p.id_producteur');
		$query->join('LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = pr.id_customer');
		$query->where('p.active = 1');
		$query->where('pr.shopoff != 1');
		$query->where('pr.valid = 1');
		$query->where('c.active = 1');
        return Db::getInstance()->getValue($query);
	}

	public function nliens(){
		$query = new DbQuery();
        $query->select('COUNT(p.id_product)');
        $query->from('product', 'p');
		$query->join('LEFT JOIN '._DB_PREFIX_.'producteur pr ON pr.id_producteur = p.id_producteur');
		$query->join('LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = pr.id_customer');
		$query->where('p.active = 1');
		$query->where('pr.shopoff != 1');
		$query->where('pr.valid = 1');
		$query->where('c.active = 1');
		// $query->where('p.link_ref IS NOT NULL');
        return Db::getInstance()->getValue($query);
	}

	public function nproducteurs(){
		$query = new DbQuery();
        $query->select('COUNT(pr.id_producteur)');
        $query->from('producteur', 'pr');
		$query->join('LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = pr.id_customer');
		$query->where('pr.valid = 1');
		$query->where('c.active = 1');
        return Db::getInstance()->getValue($query);
	}

	public function nboutique(){
		$query = new DbQuery();
        $query->select('DISTINCT pr.id_producteur');
        $query->from('producteur', 'pr');
		$query->join('LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = pr.id_customer');
		$query->join('LEFT JOIN '._DB_PREFIX_.'product p ON p.id_producteur = pr.id_producteur');
		$query->where('pr.valid = 1');
		$query->where('pr.shopoff != 1');
		$query->where('c.active = 1');
		$query->where('p.active = 1');
        $results=Db::getInstance()->ExecuteS($query);

		return count($results);
	}

	public function nmini(){
		$query = new DbQuery();
        $query->select('COUNT(pr.id_producteur)');
        $query->from('producteur', 'pr');
		$query->join('LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer = pr.id_customer');
		$query->where('pr.valid = 1');
		$query->where('c.active = 1');
		$query->where('pr.prospect = 1');
        return Db::getInstance()->getValue($query);
	}

}
