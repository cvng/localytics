<?php
/**
 * Localytics - Statistics Module
 *
 * @author    Cedric VANGOUT <vangout.cedric@gmail.com>
 * @copyright 2013-2014 cVNG IE
 * @link      https://bitbucket.org/cvng/localytics
 */

require_once(__DIR__.'/gapi.class.php');
require_once(__DIR__.'/utils.php');

class LocalyticsApi extends gapi
{
  /** @const GA Query filter for all profile visits */
  const ALL_PROFILE_FILTER = 'ga:pagePath=~^/[a-z0-9-]+$';

  /** @const GA Query filter for all shop visits */
  const ALL_SHOP_FILTER = 'ga:pagePath=@/boutique';

  /** @const GA Query filter for all product visits */
  const ALL_PRODUCT_FILTER = 'ga:pagePath=~[0-9]+-[a-zA-Z0-9-]+.html';

  /** @var int Scope reporting for automatic date range calculation */
  private $reporting_scope = null;

  /** @var Array containing start/end date for API queries */
  private $date_range = array();

  /** @var string/array containing dimensions for queries */
  private $dimensions = null;


  public function setReportingScope($reporting_scope)
  {
    $this->reporting_scope = $reporting_scope;
    $this->date_range = self::_computeDateRange($reporting_scope);
  }

  /**
   * Compute Date Range thanks to reporting scope
   *
   * @param int $reporting_scope
   * @return array containing start/end
   */
  private static function _computeDateRange($reporting_scope)
  {
    if ($reporting_scope == 1)
      $scope['start_date'] = date("Y-m-d", strtotime("-$reporting_scope month"));
    else
      $scope['start_date'] = date("Y-m-d", strtotime("first day of -$reporting_scope month"));

    $scope['end_date'] = date('Y-m-d');

    return $scope;
  }

  /**
   * Request Report Data
   *
   * @see parent::requestReportData()
   */
  public function requestReportData($dimensions=null, $metrics, $sort_metric=null, $filter=null, $start_date=null, $end_date=null, $start_index=1, $max_results=50)
  {
    if ($start_date == null OR $end_date == null) {
      $start_date = $this->date_range['start_date'];
      $end_date = $this->date_range['end_date'];
    }

    if (array_key_exists('start_date', $this->date_range) AND array_key_exists('end_date', $this->date_range))
      return parent::requestReportData($dimensions, $metrics, $sort_metric, $filter, $start_date, $end_date, $start_index, $max_results);
    else
      throw new Exception('Localytics API: You have to set $reporting_scope before any API requests (@use setReportingScope())');
  }

  /**
   * Calculate progression rate from last period
   *
   * @return float Progress rate
   */
  public function getProgressRate($filter = null)
  {
    $start_date = date("Y-m-d", strtotime($this->date_range['start_date']."-$this->reporting_scope month"));
    $end_date = date("Y-m-d", strtotime($this->date_range['end_date']."-$this->reporting_scope month"));

    $current_visits = $this->getMetrics();
    $current_visits = $current_visits[key($current_visits)];

    $this->requestReportData(null, key($this->getMetrics()), null, $filter, $start_date, $end_date);
    $last_period_visits = $this->getMetrics();
    $last_period_visits = $last_period_visits[key($last_period_visits)];

    if (!empty($last_period_visits))
      return (($current_visits - $last_period_visits) / $last_period_visits) * 100;

    return 0;
  }

  /**
   * Write XML file for chart display
   *
   * @param array $results gapiReportEntry array to put into XML file
   * @param string $src Input filename
   * @param string $dest Output filename (had to be writable !)
   */
  public static function outputResultsAsXML($results, $src, $dest)
  {
    $xml_file = simplexml_load_file($src);

    if (!empty($results[0]->getDimesions()['week'])) { // Monthly report
      // $xml_file->axis_category->addAttribute('skip', 6);
      $is_monthly_report = true;
    }

    $getter = 'get'.key($results[0]->getMetrics());

    foreach ($results as $key => $report_entry) {
      if (isset($is_monthly_report)) {
        $wk = new Datetime();
        $wk->setISODate($report_entry->getYear(), $report_entry->getWeek());

        $chart_date = strftime('%d %b', $wk->getTimestamp());
        $tooltip_date = strftime('%A %d %B %Y', $wk->getTimestamp());
      }
      else {
        $chart_date = strftime('%B', mktime(0, 0, 0, $report_entry->getMonth()));
        $tooltip_date = strftime('%B %Y', mktime(0, 0, 0, $report_entry->getMonth(), 0, $report_entry->getYear()));
      }

      $xml_file->chart_data->row[0]->addChild('string', ucwords($chart_date));
      $xml_file->chart_data->row[1]->addChild('number', $report_entry->{$getter}());

      $xml_file->chart_data->row[1]->number[$key]->addAttribute('tooltip', ucwords($tooltip_date).'\rVisites : '.$report_entry->{$getter}());
    }

    $xml_file->asXML($dest);
  }
}
