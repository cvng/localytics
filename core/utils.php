<?php
/**
 * Localytics - Statistics Module
 *
 * @author    Cedric VANGOUT <vangout.cedric@gmail.com>
 * @copyright 2013-2014 cVNG IE
 * @link      https://bitbucket.org/cvng/localytics
 */

class Utils
{
  /**
   * Find all producer links
   *
   * @param  boolean $strict_mode Escape results from empty rows
   * @param  boolean $include_logo Include logo src
   * @return array
   */
  public static function findAllProducerLinks($strict_mode = true, $include_logo = true, $sort_by_logo = true)
  {
    $query = new DbQuery();

    $query->select('id_producteur')
          ->select('link_url')
          ->select('name')
          ->from('producteur', 'p');
    if ($strict_mode) {
      $query->where('p.name IS NOT NULL')
            ->where('p.link_url IS NOT NULL');
    }
    $query->orderBy('name');

    $results = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($query);

    if ($include_logo) {
      foreach ($results as $i => $result) {
        $logo_src = Utils::getProducerLogo($result['id_producteur']);
        $results[$i]['logo'] = $logo_src ? $logo_src : false;
      }

      /**
       * @todo Sort producers with logo first in autocomplete list properly
       */
      if ($sort_by_logo) {
        $with_logo = array();
        $without_logo = array();

        foreach ($results as $result) {
          if ($result['logo'])
            $with_logo[] = $result;
          else
            $without_logo[] = $result;
        }

        return array_merge($with_logo, $without_logo);
      }
    }

    if ($results)
      return $results;
    return array();
  }

  /**
   * Find Producer ID by link
   *
   * @param  string $link Link to find
   * @return int
   */
  public static function findProducerIdByLink($link)
  {
    $query = new DbQuery();

    $query->select('id_producteur')
          ->from('producteur', 'p')
          ->where('p.link_url = \''.pSQL($link).'\'');

    if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query))
      return (int)$result;
    return false;
  }

  /**
   * Find Product ID by link
   *
   * @param  string $link Link to find
   * @return int
   */
  public static function findProductIdByLink($link)
  {
    $query = new DbQuery();

    $query->select('id_product')
          ->from('product_lang', 'pl')
          ->where('pl.link_rewrite = \''.pSQL($link).'\'');

    if ($result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query))
      return (int)$result;
    return false;
  }

  /**
   * Find Producer logo
   *
   * @param  int $id_producer
   * @return string Img source path
   */
  public static function getProducerLogo($id_producer)
  {
    $producer = new Producteur($id_producer);
    $img_src = 'marketplace/logo/'.$id_producer.'-'.$producer->link_url.'.jpg';

    return file_exists(_PS_UPLOAD_DIR_.$img_src) ? _THEME_PROD_PIC_DIR_.$img_src : false;
  }


  /**
   * Extract Producteur::link_rewrite from ga:pagePath
   *
   * @param   string $link_type : PRODUCT, PROFILE, SHOP
   * @example case PRODUCT : /category/123-my-product.html => my-product
   * @example case PROFILE : /my-profile => my-profile
   * @example case SHOP : /my-profile/boutique => my-profile
   *
   * @param   string $page_path
   * @return  string
   */
  public static function extractLink($page_path, $link_type)
  {
    switch ($link_type) {
      case 'PRODUCT':
        return str_replace('.html', '', preg_replace('/\/[0-9]+-/', '', preg_replace('/\/[a-z]+/', '', $page_path)));
        break;

      case 'PROFILE':
        return str_replace('/', '', $page_path);
        break;

      case 'SHOP':
        return str_replace(array('/', 'boutique'), '', $page_path);
        break;

      default:
        return false;
        break;
    }
  }

  /**
   * Find most attractive products categories
   * Totalize category who appears mostly
   *
   * @param  array $products_array
   * @return array
   */
  public static function findMostAttractiveCategories($products_array, $category_level = false)
  {
    $categories = array();
    $best_categories = array();

    foreach ($products_array as $product) {
      $product_categories = Product::getProductCategories($product['id']);

      $categories[] = self::getParentCategory($product_categories[0], 2, false);
    }

    $best_categories = array_count_values($categories);
    arsort($best_categories);

    return $best_categories;
  }

  /**
   * Get parent category by level depth
   *
   * @param  int $id_category
   * @param  int $level Level depth
   * @return object Parent category
   */
  public static function getParentCategory($id_category, $level = 1, $return_object = true)
  {
    $category = new Category($id_category);
    $parents_categories = $category->getParentsCategories();

    foreach ($parents_categories as $parent_cat) {
      if ($parent_cat['level_depth'] == $level)
        return $return_object ? new Category($parent_cat['id_category']) : $parent_cat['id_category'];
    }
    return false;
  }
}

