{**
 * Localytics - Statistics Module
 *
 * @author    Cedric VANGOUT <vangout.cedric@gmail.com>
 * @copyright 2013-2014 cVNG IE
 * @link      https://bitbucket.org/cvng/localytics
 *}

{literal}
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/themes/humanity/jquery-ui.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
  var json_data = {/literal}{$json_data|json_encode}{literal};
  var list = new Array();

  for (var i=0; i<json_data.length; i++) {
    data = new Array();

    data['value'] = json_data[i]['id_producteur'];
    data['label'] = json_data[i]['name'];
    data['logo'] = json_data[i]['logo'];

    list.push(data);
  }

  $("#search").autocomplete({
    source : list,
    select: function(event, ui) {
      $("#main-form").submit();
    }
  }).data("autocomplete")._renderItem = function (ul, item) {
    return $("<li />")
      .data("item.autocomplete", item)
      .append("<a>" + (item.logo ? "<img height='48' width='116' id='image' src='" + item.logo + "'/>" : "") +
        "<img src='{/literal}{$img_dir}{literal}admin/bullet_green.png'/> <b>" + ucwords(item.label) + "</b></a>")
      .appendTo(ul);
  };

});
</script>
{/literal}

<h2>Audience locale</h2>

<form id="main-form" action="" method="post">

  {* URL TRACKING BLOCK *}

  <fieldset>
    <legend><img src="../img/t/AdminStats.gif" />URL Tracking</legend>

    <label>Sélection :</label>

    <input type="submit" name="submitReset" value="Accueil" class="button">
    <input type="text" id="search" name="id_producer" size="50"{if !empty($id_producer)} value="{$id_producer}"{/if}/>

    {* @deprecated
    <select name="id_producer" onchange="this.form.submit()" class="form-control">
      <option value=""{if empty($id_producer)} selected="selected"{/if}>-- Audience Totale --</option>

      {foreach from=$producers item=producer}
      <option value="{$producer.id_producteur}"{if $id_producer == $producer.id_producteur} selected="selected"{/if}>{$producer.name}</option>
      {/foreach}

    </select>
    *}
    <small> / {$json_data|@count} fabricant(s)</small>

    {foreach from=$reporting_scopes key=key item=dr}
      <input type="radio" name="reporting_scope" onchange="this.form.submit()" value="{$key}"{if $reporting_scope == $key} checked="checked"{/if}/> {$dr}
    {/foreach}

    <div align="right"></div>

    <br><br>
    <div>
      {literal}
      <script type="text/javascript">
        drawChartObject();
      </script>
      {/literal}
    </div>

  <br><br>
  <table class="table" style="width:100%">

    {if empty($id_producer)}
    <tr>
      <th width="150px"><img src="../img/admin/separator_breadcrumb.png"/>Logo</th>
      <th width="100px"><img src="../img/admin/AdminStats.gif"/>Evolution</th>
      <th><img src="../img/admin/home.png" />Total visites <small>(ga:visits)</small></th>
      <th width="150px"><img src="../img/admin/AdminCustomers.gif"/>Visites profils</th>
      <th width="150px"><img src="../img/admin/AdminOrders.gif"/>Visites boutiques</th>
      <th width="150px"><img src="../img/admin/AdminPayment.gif"/>Visites produits</th>
    </tr>
    <tr>
      <td align="center"><img height="48px" width="48px" src="../img/logo.jpg" /></td>
      <td>
        <h1 title="Par rapport au {if $reporting_scope != 1}{$reporting_scope} {/if}mois précédent(s)">
          <img src="../img/admin/arrow_{if $progress_rate > 0}up{else}down{/if}.png" />{$progress_rate}%
        </h1>
      </td>
      <td><h1>{$total_visits}</h1></td>
      <td><h1>{$all_profiles_visits|default:'N/A'}</h1></td>
      <td><h1>{$all_shops_visits|default:'N/A'}</h1></td>
      <td><h1>{$all_products_visits|default:'N/A'}</h1></td>
    </tr>

  </table><br><br>

  </fieldset>

  {* DASHBOARD BLOCK *}

  <h2>Tableau de bord</h2>
  <fieldset>
    <legend><img src="../img/t/AdminStats.gif" />Dashboard</legend>
  <br><br>
  <table class="table" style="width:100%">
    <tr>
      <th>N° de téléphone</th>
      <th>Produits en ligne</th>
      <th>Liens vers fiches produits tiers</th>
      <th>Producteurs</th>
      <th>Boutiques</th>
    </tr>
    <tr>
      <td><h1>{$nb_tel|default:'N/A'}</h1></td>
      <td><h1>{$nb_products|default:'N/A'}</h1></td>
      <td><h1>{$nb_liens|default:'N/A'}</h1></td>
      <td><h1>{$nb_producteurs|default:'N/A'}</h1></td>
      <td><h1>{$nb_boutique|default:'N/A'}</h1></td>
    </tr>
    <tr>
      <th>Mini-profils</th>
      <th>Mails producteurs</th>
      <th>Visites des liens tiers</th>
      <th>Avis produits</th>
      <th>Avis producteurs</th>
    </tr>
    <tr>
      <td><h1>{$nb_mini|default:'N/A'}</h1></td>
      <td><h1>{$nb_mailprod|default:'N/A'}</h1></td>
      <td><h1>{$nb_visitslien|default:'N/A'}</h1></td>
      <td><h1>{$nb_avisprod|default:'N/A'}</h1></td>
      <td><h1>{$nb_avisproducteurs|default:'N/A'}</h1></td>
    </tr>
  </table>

  </fieldset>

    {* RANKING BLOCK *}

    <h2>Classement</h2>
    <fieldset>
      <legend><img src="../img/admin/asterisk.gif" />Ranking</legend>
      {$max_list=30}
    <label>Catégories les + récurrentes :</label>
    {foreach from=$best_categories|@array_slice:0:10 key=key item=cat}
      <b> | {$cat['name'][1]}</b>
    {/foreach}
    <br><br>
    <table class="table" style="width:100%">
      <tr>
        <th>Produits les + consultés</th>
        <th>Profils les + consultés</th>
        <th>Boutiques les + consultés</th>
      </tr>
      <tr>
        <td>
          <table>
          {foreach from=$best_products|@array_slice:0:$max_list key=key item=product}
            <tr>
              <td>
                <a href="{$product.link}"><b>[{$key + 1}]</b> {$product.producer_name|default:'N/A'} | <b>{$product.name|default:'N/A'|ucwords}</b></a> :
                <i>{$product.visits|default:'N/A'}</i> visites
              </td>
              <td>
                {$product.category|default:'N/A'|ucwords}
              </td>
            </tr>
          {/foreach}
          </table>
        </td>

        <td><table>
        {foreach from=$best_profiles|@array_slice:0:$max_list key=key item=profile}
          <tr>
            <td>
              <a href="{$profile.id}"><b>[{$key + 1}]</b> <b>{$profile.name|default:'N/A'|ucwords}</b> :
                <i>{$profile.visits|default:'N/A'} visites</i></a>
            </td>
            <td>
              {if $profile.has_phone}<img src="../img/admin/home.png"/>{/if}{if $profile.has_shop}<img src="../img/admin/cart_addons.png"/>{/if}
            </td>
          </tr>
        {/foreach}
        </table></td>

        <td><table>
        {foreach from=$best_shops|@array_slice:0:$max_list key=key item=shop}
          <tr>
            <td>
              <a href="{$shop.id}"><b>[{$key + 1}]</b> {$shop.name|default:'N/A'} : <i>{$shop.visits|default:'N/A'} visites.</i></a>
            </td>
            <td>
              {$shop.nb_products} produit(s)
            </td>
          </tr>
          {/foreach}
        </table></td>
      </tr>

    </fieldset>

    {else}

    <table class="table" style="width:100%">
    <tr>
      <th width="150px"><img src="../img/admin/separator_breadcrumb.png" />Logo</th>
      <th width="100px"><img src="../img/admin/AdminStats.gif" />Evolution</th>
      <th><img src="../img/admin/home.png" />Total pages vues <small>(ga:pageviews)</small></th>
      <th width="80px"><img src="../img/admin/AdminCustomers.gif" />Profil</th>
      <th width="80px"><img src="../img/admin/AdminOrders.gif" />Boutique</th>
      <th width="80px"><img src="../img/admin/AdminPayment.gif" />Produits</th>
    </tr>
    <tr>
      <td align="center">
        <img height="48px" {if $logo_src}src="{$logo_src}" width="116px"{else}src="{$img_dir}m/fr-default-medium_default.jpg" width="48"{/if}/>
      </td>
      <td>
        <h1 title="Par rapport au {if $reporting_scope != 1}{$reporting_scope} {/if}mois précédent(s)">
          <img src="../img/admin/arrow_{if $progress_rate > 0}up{else}down{/if}.png" />{$progress_rate}%
        </h1>
      </td>
      <td><h1>{$total_visits|default:'N/A'}</h1></td>
      <td><h1>{$profile_visits|default:'N/A'}</h1></td>
      <td><h1>{$shop_visits|default:'N/A'}</h1></td>
      <td><h1>{$products_visits|default:'N/A'}</h1></td>
    </tr>

  </table>

  <br><br>
  <fieldset>
    <legend><img src="../img/t/AdminStats.gif" />Product Tracking</legend>
    <label>Sélection :</label>
    <select name="id_product" onchange="this.form.submit()">
          <option value=""> -- </option>
          {foreach from=$products item=product}
          <option value="{$product.id_product}" {if $id_product == $product.id_product}selected="selected"{/if}>{$product.name}</option>
          {/foreach}

    </select><small> / {$products|@count} produit(s)</small>
  </fieldset>

  {if !empty($id_product)}
  <br><br>
  <table class="table" style="width:100%">
    <tr>
      <th width="150px">Aperçu</th>
      <th width="100px">Evolution</th>
      <th>Vues produits <small>(ga:pageviews)</small></th>
    </tr>
    <tr>
      <td align="center"><img width="64px" height="64px" src="{if isset($product_img_src)}{$product_img_src}{else}{$img_dir}m/fr-default-medium_default.jpg{/if}"></td>
      <td>
        <h1 title="Par rapport au {if $reporting_scope != 1}{$reporting_scope} {/if}mois précédent(s)">
          <img src="../img/admin/arrow_{if $product_prog_rate > 0}up{else}down{/if}.png" />{$product_prog_rate|default:'N/A'}%
        </h1>
      </td>
      <td><h1>{$product_visits|default:'N/A'}</h1></td>
    </tr>
    {/if}

    {/if}

  </table>
  <br><br>

</form>

