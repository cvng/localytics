{if $errors|@count > 0}
  <div class="error">
    <ul>
      {foreach from=$errors item=error}
        <li>{$error}</li>
      {/foreach}
    </ul>
  </div>
{/if}

<form action="{$request_uri}" method="post">
  <fieldset>
    <legend><img src="{$path}icon.png" alt="" title="" />{l s='Configuration' mod='localytics'}</legend>
    <label>{l s='Connexion à votre compte Google : ' mod='localytics'}</label>
    <div class="margin-form">
      <input type="text" size="20" name="GAPI_EMAIL" value="{$GAPI_EMAIL}" />
      <input type="password" size="20" name="GAPI_PASSWD" value="{$GAPI_PASSWD}" />
      <p class="clear">{l s='Connectez-vous avec un compte Google lié au profil Analytics.' mod='localytics'}</p>
    </div>
    <center><input type="submit" name="{$submitName}" value="{l s='Save' mod='localytics'}" class="button" /></center>
  </fieldset>
</form>
