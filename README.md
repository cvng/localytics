README
======
[Localytics](https://bitbucket.org/cvng/localytics/ "Localytics")

ABOUT
--------

Localytics is an additional statistics module for PrestaShop 1.5 based on marketplace abilities.


REQUIREMENTS
--------

No requirements since last major update. This module extends GAPI.

Visit [Project page][1] if needed.


USAGE
--------

One-click-install via PrestaShop back-office.
Once installed you will have to log-in via a Google Account.

If a warning appears, you may have forgot your authentication credentials in module settings page.
For any support, contact dev : [vangout.cedric@gmail.com][2].


Thank you for your confidence !

[1]: https://code.google.com/p/gapi-google-analytics-php-interface/
[2]: vangout.cedric@gmail.com
